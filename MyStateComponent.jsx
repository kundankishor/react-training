import React from "react";
import ContentComponentLife from "./TestComponetnLife.jsx";
import Button from "react-bootstrap/Button";

class MyStateComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
    this.updateState = this.updateState.bind(this);
  }

  updateState() {
    console.log("State Updated");
    var item = "setTime..." + Date.now(); // take dynamic value form object
    var myArray = this.state.data;
    myArray.push(item);
    // setState is an implicit event in React, used to set the current state
    this.setState({ data: myArray });
  }

  render() {
    return (
      <div>
        <Button variant="primary" onClick={this.updateState}>
          Updated state
        </Button>
        <ContentComponentLife myNumber={this.state.data} />
      </div>
    );
  }
}
export default MyStateComponent;
