import React from "react";
import { Router, Route, Link, browserHistory, IndexRoute } from "react-router";

class AppRouter extends React.Component {
  render() {
    return (
      <div>
        <h1>Router Example</h1>

        <Link to="home">Home!</Link>
        <Link to="about">About Us !</Link>
        <Link to="contact">Contact Us !</Link>
        <Link to="customForm">User Form !</Link>
        <Link to="apiCall">API !</Link>

        {this.props.children}
      </div>
    );
  }
}

export default AppRouter;
