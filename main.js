import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import Employee from './MyComponentTwo.jsx';
import MyStateComponent from './MyStateComponent.jsx';
import MyForm from './MyForm.jsx';
import FetchDemo from './FetchDemo.jsx';

import Home from "./Home.jsx";
import About from "./About.jsx";
import Contact from "./Contact.jsx";
import AppRouter from "./AppRoute.jsx";

import { Router, Route, Link, browserHistory, IndexRoute  } from 'react-router';


let emp = {
    name: 'Kundan',
    city: 'DXB'
};
ReactDOM.render((
<Router history = {browserHistory}>
    <Route path = "/" component = {AppRouter}>
           <IndexRoute component = {Home} />
           <Route path = "home" component = {Home} />
           <Route path = "about" component = {About} />
           <Route path = "contact" component = {Contact} />
           <Route path = "customForm" component = {MyForm} />
           <Route path = "apiCall" component = {() => <FetchDemo subreddit = "reactjs" />} />
    </Route>
   </Router>
   ), document.getElementById('router'));


   
// ReactDOM.render( < App name = "Kundan" / > , document.getElementById('app')); {
//     /* ReactDOM.render( < Employee emp = {
//                 emp
//             }
//             />, document.getElementById('comp')); */
// }
// ReactDOM.render( < MyStateComponent / > , document.getElementById('state'));
// ReactDOM.render( < MyForm / > , document.getElementById('myForm'));
// ReactDOM.render( < FetchDemo subreddit = "reactjs" / > , document.getElementById('fetch'));



