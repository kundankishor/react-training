import React from "react";

class App extends React.Component {
  //   constructor(props) {
  //     super(props);
  //     this.state = {
  //       cssStyle: { fontSize: 50, color: "#000000" }
  //     };
  //   }
  render() {
    let myStyle = {
      fontSize: 50,
      color: "#FF0000"
    };
    let aircodes = ["DXB", "LHR", "SIN", "PNQ"];

    return (
      <div>
        <h1 style={myStyle}>Application React</h1>
        <h1>My username --{this.props.name}</h1>
        <ul>
          {aircodes.map(function(company, index) {
            return <li key={index}> {company} </li>;
          })}
        </ul>
      </div>
    );
  }
}

export default App;
